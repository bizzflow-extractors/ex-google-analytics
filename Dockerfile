FROM python:3.8-slim
RUN python3 -m pip install --upgrade pip && python3 -m pip install --upgrade setuptools
RUN python3 -m pip install google-api-python-client oauth2client
ADD requirements.txt /requirements.txt
RUN python3 -m pip install -r /requirements.txt

VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python3", "main.py"]
