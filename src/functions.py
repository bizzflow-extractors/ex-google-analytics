# pylint: disable=wrong-import-order,missing-docstring
import pandas as pd
from httplib2 import HttpLib2Error
import googleapiclient.errors
from service import service, conf
from datetime import datetime, timedelta
from dateutil.rrule import rrule, DAILY
from time import sleep
from retry_helper import RetryManager
import os
import inspect
import csv


# Print debug information (if configured)
def debug(o):
    if conf["debug"]:
        print(o)

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)
    return next_month - timedelta(days=next_month.day)

def first_day_of_current_month(any_day):
    first_date_this_month = any_day.replace(day=1) 
    return first_date_this_month

def last_day_of_current_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)
    last_day = next_month - timedelta(days=next_month.day)
    if last_day > datetime.today():
        last_day = datetime.today()
    return last_day

def first_day_of_last_month(any_day):
    first_date = any_day.replace(day=1)
    lastMonth = first_date - timedelta(days=1)
    fday_lmonth = first_day_of_current_month(lastMonth)
    return fday_lmonth

def last_day_of_last_month(any_day):
    first_date = any_day.replace(day=1)
    lastMonth = first_date - timedelta(days=1)
    lday_lmonth = last_day_of_current_month(lastMonth)
    return lday_lmonth

def convert_reponse_to_df(response):
    lst = []
    # parse report data
    for report in response.get('reports', []):

        columnHeader = report.get('columnHeader', {})
        dimensionHeaders = columnHeader.get('dimensions', [])
        metricHeaders = columnHeader.get(
            'metricHeader', {}).get('metricHeaderEntries', [])
        rows = report.get('data', {}).get('rows', [])

        for row in rows:
            dct = {}
            dimensions = row.get('dimensions', [])
            dateRangeValues = row.get('metrics', [])

            for header, dimension in zip(dimensionHeaders, dimensions):
                dct[header] = dimension

            for i, values in enumerate(dateRangeValues):
                for metric, value in zip(metricHeaders, values.get('values')):
                    if '.' in value or ',' in value:
                        dct[metric.get('name')] = float(value)
                    else:
                        dct[metric.get('name')] = int(value)
            lst.append(dct)

        df = pd.DataFrame(lst)
        return df


# according to docstring in execute for batch operation catch HttpLib2Error
# this should occur when something wrong is with transportation
# in other API error cases can raised also google HttpError - but probably it doesn't make sense to retry it
# @RetryManager(max_attempts=3, wait_seconds=1, exceptions=(HttpLib2Error, googleapiclient.errors.HttpError))
def get_report(analytics, start_date, end_date, view_id, metrics, dimensions, dimensionFilterClauses=None, segments=None):
    if segments is None:
        segments = []
    if dimensionFilterClauses is None:
        dimensionFilterClauses = []
    with RetryManager(max_attempts=3, wait_seconds=10, exceptions=(HttpLib2Error, googleapiclient.errors.HttpError)) as retry:
        while retry:
            with retry.attempt:
                return analytics.reports().batchGet(
                    body={
                        'reportRequests': [
                            {
                                'viewId': view_id,
                                'dateRanges': [{'startDate': start_date, 'endDate': end_date}],
                                'metrics': metrics,
                                'dimensions': dimensions,
                                'pageSize': 100000,
                                'dimensionFilterClauses': dimensionFilterClauses,
                                'segments': segments,
                            }]
                    }
                ).execute()


def return_ga_data(start_date, end_date, view_id, metrics, dimensions, split_dates, group_by=None, dimensionFilterClauses=None, segments=None):
    if group_by is None:
        group_by = []
    if dimensionFilterClauses is None:
        dimensionFilterClauses = []
    if segments is None:
        segments = []
    if split_dates == False:
        return convert_reponse_to_df(get_report(service, start_date, end_date, view_id, metrics, dimensions, dimensionFilterClauses, segments))
    start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

    df_total = pd.DataFrame()
    for date in rrule(freq=DAILY, dtstart=start_date, until=end_date):
        date = str(date.date())
        df_total = df_total.append(convert_reponse_to_df(get_report(
            service, date, date, view_id, metrics, dimensions, dimensionFilterClauses, segments)))
        sleep(1)

    if len(group_by) != 0:
        df_total = df_total.groupby(group_by).sum()

    return df_total

# Get the current folder of running script


def get_current_folder():
    current_folder = os.path.dirname(os.path.abspath(
        inspect.getfile(inspect.currentframe()))) + '/'
    return current_folder

# Create target Directory if don't exist


def create_directory(dirName):
    if not os.path.exists(dirName):
        os.mkdir(dirName)
        debug("Directory " + dirName + " Created ")
    else:
        debug("Directory " + dirName + " already exists")

# Save dataframe to csv


def save_df_to_csv(df, path, file_name):
    create_directory(path)
    output_filename = os.path.join(path, file_name+'.csv')
    df.to_csv(output_filename, index=False, header=True, encoding='utf-8', quoting=csv.QUOTE_ALL)
    debug("File: {} successfully saved".format(output_filename))

# Save dataframe to excel


def save_df_to_excel(df, path, file_name, sheet_name):
    create_directory(path)
    output_filename = os.path.join(path, file_name+'.xlsx')
    writer = pd.ExcelWriter(output_filename, engine='xlsxwriter')
    df.to_excel(writer, sheet_name=sheet_name)
    writer.save()
    debug("File: {} successfully saved".format(output_filename))
