# import libraries
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import os
import inspect
import json

CONFIG_LOCATION = "/config/config.json" if os.path.exists("/config/config.json") else "config.json"

# Load configuration
if not os.path.exists(CONFIG_LOCATION):
    raise Exception("Configuration not specified")

with open(CONFIG_LOCATION) as conf_file:
    conf = json.loads(conf_file.read())

if isinstance(conf["service_account"],str):
    conf["service_account"] = json.loads(conf["service_account"])


# create connection based on project credentials
credentials = ServiceAccountCredentials.from_json_keyfile_dict(
    conf["service_account"])
service = build(conf["api_name"], conf["api_version"], credentials=credentials)
