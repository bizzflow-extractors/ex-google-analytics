import logging
from datetime import datetime
from time import sleep
from functions import return_ga_data, get_current_folder, save_df_to_csv, save_df_to_excel, last_day_of_month
from functions import first_day_of_current_month, last_day_of_current_month, first_day_of_last_month, last_day_of_last_month
from dateutil.relativedelta import relativedelta
from service import conf

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

for request in conf["requests"]:
    logger.info("Processing request %s", request.get("name"))
    if request["end_date"] == "today_split":
        start_date = datetime.strptime(request["start_date"], "%Y-%m-%d")
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        df = None
        while True:
            sleep(1.5)
            end_date = last_day_of_month(start_date)
            if end_date > today:
                end_date = today
            sdate = start_date.strftime("%Y-%m-%d")
            edate = end_date.strftime("%Y-%m-%d")
            print("Getting data for date range: ", sdate, "=>", edate)
            current = return_ga_data(
                start_date=sdate,
                end_date=edate,
                view_id=request["view_id"],
                metrics=request["metrics"],
                dimensions=request["dimensions"],
                split_dates=request["split_dates"],
                group_by=request["group_by"],
                dimensionFilterClauses=request["dimensionFilterClauses"],
                segments=request["segments"]
            )
            if df is None:
                df = current
            else:
                df = df.append(current)
            if end_date >= today:
                break
            start_date = start_date + relativedelta(months=1)

    elif request["start_date"] == "thisMonth":
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        fdate = first_day_of_current_month(today)
        ldate = last_day_of_current_month(today)
        sdate = fdate.strftime("%Y-%m-%d")
        edate = ldate.strftime("%Y-%m-%d")
        df = return_ga_data(
                start_date=sdate,
                end_date=edate,
                view_id=request["view_id"],
                metrics=request["metrics"],
                dimensions=request["dimensions"],
                split_dates=request["split_dates"],
                group_by=request["group_by"],
                dimensionFilterClauses=request["dimensionFilterClauses"],
                segments=request["segments"]
            )

    elif request["start_date"] == "lastMonth":
        today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        fdate = first_day_of_last_month(today)
        ldate = last_day_of_last_month(today)
        sdate = fdate.strftime("%Y-%m-%d")
        edate = ldate.strftime("%Y-%m-%d")
        df = return_ga_data(
                start_date=sdate,
                end_date=edate,
                view_id=request["view_id"],
                metrics=request["metrics"],
                dimensions=request["dimensions"],
                split_dates=request["split_dates"],
                group_by=request["group_by"],
                dimensionFilterClauses=request["dimensionFilterClauses"],
                segments=request["segments"]
            )

    else:
        df = return_ga_data(
                start_date=request["start_date"],
                end_date=request["end_date"],
                view_id=request["view_id"],
                metrics=request["metrics"],
                dimensions=request["dimensions"],
                split_dates=request["split_dates"],
                group_by=request["group_by"],
                dimensionFilterClauses=request["dimensionFilterClauses"],
                segments=request["segments"]
            )
    save_df_to_csv(
        df=df,
        #path= get_current_folder() + "/test_reports/",
        path=conf["output_directory"],
        file_name=request["name"]
    )
