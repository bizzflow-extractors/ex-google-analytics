# Google Analytics Extractor

This repository contains the dockerized python 3 extractor for Google Analytics.
The Analytics Core Reporting API returns a maximum of 100,000 rows per request, no matter how many you ask for. If you need more, use the set the `split_dates` parameter in the config gile to `true`.

Required Py libraries: 
- `googleapiclient`
- `oauth2client`
- `pandas`


## Config File manage everything
The config file is encrypted so if you would like to change the configuration you have to decrypt, change and then again encrypt.

### Config sample
```json
{
    "output_directory": "/data/out/tables",
    "debug": true,
    "scopes": [
        "https://www.googleapis.com/auth/analytics.readonly"
    ],
    "api_name": "analyticsreporting",
    "api_version": "v4",
    "service_account": {
        "type": "",
        "project_id": "",
        "private_key_id": "",
        "private_key": "",
        "client_email": "",
        "client_id": "",
        "auth_uri": "",
        "token_uri": "",
        "auth_provider_x509_cert_url": "",
        "client_x509_cert_url": ""

    },
    "requests": [{
        "name": "ga_acquisition_data",
        "start_date": "2017-01-01",
        "end_date": "today",
        "view_id": "",
        "metrics": [{
            "expression": "ga:sessions"
        },{
            "expression": "ga:pageviews"
        },{
            "expression": "ga:users"
        },{
            "expression": "ga:newUsers"
        },{
            "expression": "ga:transactions"
        },{
            "expression": "ga:transactionRevenue"
        },{
            "expression": "ga:avgSessionDuration"
        },{
            "expression": "ga:sessionDuration"
        },{
            "expression": "ga:bounces"
        },{
            "expression": "ga:timeOnPage"
        }],
        "dimensions": [{ "name": "ga:date" },
            { "name": "ga:sourceMedium" },
            { "name": "ga:source" },
            { "name": "ga:landingPagePath" },
            { "name": "ga:channelGrouping" },
            { "name": "ga:deviceCategory" },
            { "name": "ga:campaign" },
            { "name": "ga:country" }
        ],
        "split_dates": false,
        "group_by": [],
        "dimensionFilterClauses": [],
        "segments": []
    }]
}
```

### Set up the configuration for connection:
'''
"scopes":[scopes],
"api_name":"api_name",
"api_version":"api_version",
"service_account":{your_service_account_key},
'''

### Set up requests:

A brief description of each parameter of one request:

- `start_date` & `end_date`:
  - date format in `'YYYY-MM-DD'`
  - relative date: `'today'`, `'yesterday'`, `'NdaysAgo'` (where N is the amount of days) <--- only when `split_dates = false`
  - alternatively, `end_date` may be set to `today_split`, for each month between `start_date` and 
  `end_date`, separate requests will be sent
  - also you can set `start_date` to `thisMonth` to get all the data for actual month or `lastMonth` for getting the data for last month: 
    - e.g. if today is 2020-08-03 
        - thisMonth will get data from 2020-08-01 to 2020-08-03
        - lastMonth will get data from 2020-07-01 to 2020-07-31
        - `if you use lastMonth or thisMonth in start_date, it does not matter what you will put in the end_date`
- `view_id`: the ID of the Google Analytics view you want to import data from.
- `metrics`: the list of sessions you want to import (max. 10) - full list [here](https://developers.google.com/analytics/devguides/reporting/core/dimsmets).
- `dimensions`: the list of dimensions you want to import (max. 9) - full list [here](https://developers.google.com/analytics/devguides/reporting/core/dimsmets).
- `split_dates`: boolean. If true each day in your date range is queries seperately and merged into a data frame later on.
- `group_by` (optional): if you enable `split_dates` you can group the data on a dimension of choice. Especially handy when you're not include the date in your export.
- `dimensionFilterClauses` (optional): filter data based on dimensions if required ([see documention here](https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet#DimensionFilterClause).
- `segments` (optional): use to apply segments ([see example here](https://developers.google.com/analytics/devguides/reporting/core/v4/samples#segments)). Requires the dimension `ga:segment` in your query.

Example of request:
'''
{
        "name":"test1",
        "start_date":"30daysAgo",
        "end_date":"today",
        "view_id":"166684195",
        "metrics":[ {"expression": "ga:sessions"} ],
        "dimensions":[ {"name": "ga:source"} ],
        "split_dates":false,
        "group_by":["ga:source"],
        "dimensionFilterClauses":[],
        "segments":[]
}
'''

## Export to CSV or EXCEL

In functions.py are two functions - save_df_to_csv and save_df_to_excel. Both could be used in main.py for loop to save the data in required file format.
